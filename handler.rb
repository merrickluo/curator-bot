# frozen_string_literal: true

load 'vendor/bundle/bundler/setup.rb'

require 'telegram/bot'
require 'telegram/bot/types'
require 'yaml'

CONFIG = YAML.load_file("config.yml")

Telegram.bots_config = {
  default: ENV['TG_TOKEN']
}

def process(event:, context:)
  body = JSON.parse(event['body'])
  update = Telegram::Bot::Types::Update.new(body)
  if update.message
    answer_message(update.message)
  elsif update.inline_query
    answer_inline_query(update.inline_query)
  end
  { statusCode: 200 }
rescue Telegram::Bot::Error => e
  puts e
  { statusCode: 200 }
end

def answer_message(message)
  p 'answering message'
  Telegram.bot.send_message(
    chat_id: message.chat.id,
    text: build_msg(message.text || 'nil')
  )
end

def answer_inline_query(query)
  p 'answering inline query'
  return if query.query.nil? || query.query.empty?

  text = query.query

  results = build_inline_results(
    CONFIG['memes'].map do |meme|
      { title: "#{text} #{meme['suffix']}", text: build_msg(text, meme) }
    end
  )

  Telegram.bot.answer_inline_query(
    inline_query_id: query.id,
    results: results
  )
end

def build_inline_results(results)
  results.map do |result|
    {
      type: 'article',
      id: result[:title].hash.to_s,
      title: result[:title],
      input_message_content: {
        message_text: result[:text]
      }
    }
  end
end

def build_msg(topic, meme=nil)
  meme ||= default_meme

  content = meme['text'] % { text: topic }
  return content if meme['disable_footer']

  footer = CONFIG['footer'] % { text: topic }
  "#{content}#{footer}"
end

def default_meme
  CONFIG['memes'].find{ |m| m['name'] == 'default' }
end
